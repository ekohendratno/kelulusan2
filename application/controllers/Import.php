<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include APPPATH.'third_party/PHPExcel/PHPExcel.php';

class Import extends CI_Controller {
    function __construct() {
        parent::__construct();

		$this->load->model('Mymodel','m');
		$this->load->helpers('form');
		$this->load->helpers('url');
		
		if( empty($this->session->userdata('level')) ){
			redirect('home');
		}
		
		$this->uid = $this->session->userdata('uid');

    }



    function siswa() {

		$idx_baris_mulai = 3;
		$idx_baris_selesai = 1000;

		$target_file = './uploads/temp/';
		$buat_folder_temp = !is_dir($target_file) ? @mkdir("./uploads/temp/") : false;

		move_uploaded_file($_FILES["file"]["tmp_name"], $target_file.$_FILES['file']['name']);

		$file   = explode('.',$_FILES['file']['name']);
		$length = count($file);

		$result = array();
		$data = array();
		if($file[$length -1] == 'xlsx' || $file[$length -1] == 'xls') {

			$tmp    = './uploads/temp/'.$_FILES['file']['name'];
			//Baca dari tmp folder jadi file ga perlu jadi sampah di server :-p

			$read   = PHPExcel_IOFactory::createReaderForFile($tmp);
			$read->setReadDataOnly(true);
			$excel  = $read->load($tmp);

			$_sheet_bismen = $excel->setActiveSheetIndexByName('bismen');

			$result['pesan'] = "Format template tidak sesuai";
			for ($j = $idx_baris_mulai; $j <= $idx_baris_selesai; $j++) {

				$_b = $_sheet_bismen->getCell("B".$j)->getCalculatedValue();
				$_c = $_sheet_bismen->getCell("C".$j)->getCalculatedValue();
				$_d = $_sheet_bismen->getCell("D".$j)->getCalculatedValue();
				$_e = $_sheet_bismen->getCell("E".$j)->getCalculatedValue();
				$_f = $_sheet_bismen->getCell("F".$j)->getCalculatedValue();
				$_g = $_sheet_bismen->getCell("G".$j)->getCalculatedValue();
				$_h = $_sheet_bismen->getCell("H".$j)->getCalculatedValue();
				$_i = $_sheet_bismen->getCell("I".$j)->getCalculatedValue();
				$_j = $_sheet_bismen->getCell("J".$j)->getCalculatedValue();
				$_k = $_sheet_bismen->getCell("K".$j)->getCalculatedValue();
				$_l = $_sheet_bismen->getCell("L".$j)->getCalculatedValue();
				$_m = $_sheet_bismen->getCell("M".$j)->getCalculatedValue();
				$_n = $_sheet_bismen->getCell("N".$j)->getCalculatedValue();
				$_o = $_sheet_bismen->getCell("O".$j)->getCalculatedValue();
				$_p = $_sheet_bismen->getCell("P".$j)->getCalculatedValue();
				$_q = $_sheet_bismen->getCell("Q".$j)->getCalculatedValue();
				$_r = $_sheet_bismen->getCell("R".$j)->getCalculatedValue();
				$_s = $_sheet_bismen->getCell("S".$j)->getCalculatedValue();
				$_t = $_sheet_bismen->getCell("T".$j)->getCalculatedValue();
				$_u = $_sheet_bismen->getCell("U".$j)->getCalculatedValue();
				$_v = $_sheet_bismen->getCell("V".$j)->getCalculatedValue();
				$_w = $_sheet_bismen->getCell("W".$j)->getCalculatedValue();

				if ( $_b != "" ) {
					$item = array();
					$item['peserta_ttl'] = $_c;
					$item['peserta_nama'] = strtoupper( $_b );
					$item['peserta_noinduk'] = $_d;
					$item['peserta_nisn'] = $_e;

					$item['peserta_jk'] = "";
					//$item['peserta_password'] = "1234";
					$item['peserta_jurusan'] = strtoupper( $this->_replaceJurusan( $_f ) );

					$item['peserta_nilai_agama'] = $_g;
					$item['peserta_nilai_pkn'] = $_h;
					$item['peserta_nilai_bindo'] = $_i;
					$item['peserta_nilai_mtk'] = $_j;
					$item['peserta_nilai_sind'] = $_k;
					$item['peserta_nilai_bing'] = $_l;

					$item['peserta_nilai_seni'] = $_m;
					$item['peserta_nilai_penjas'] = $_n;
					$item['peserta_nilai_blamp'] = $_o;

					$item['peserta_nilai_pak'] = $_p;
					$item['peserta_nilai_simdig'] = $_q;


					//jika bismen

					$item['peserta_nilai_ipa'] = $_r;
					$item['peserta_nilai_ekonomi'] = $_s;
					$item['peserta_nilai_administrasi'] = $_t;

					$item['peserta_nilai_c2'] = $_u;
					$item['peserta_nilai_c3'] = $_v;
					$item['peserta_nilai_rata2x'] = $_w;

					$item['peserta_jenis'] = "BISMEN";

					//jika teknik

					$item['peserta_nilai_fiska'] = "";
					$item['peserta_nilai_kimia'] = "";



					$item['peserta_tahunajaran'] = "2021/2022";

					array_push($data,$item);
				}




			}


			$_sheet_teknik = $excel->setActiveSheetIndexByName('teknik');

			for ($j = $idx_baris_mulai; $j <= $idx_baris_selesai; $j++) {

				$_b = $_sheet_teknik->getCell("B".$j)->getCalculatedValue();
				$_c = $_sheet_teknik->getCell("C".$j)->getCalculatedValue();
				$_d = $_sheet_teknik->getCell("D".$j)->getCalculatedValue();
				$_e = $_sheet_teknik->getCell("E".$j)->getCalculatedValue();
				$_f = $_sheet_teknik->getCell("F".$j)->getCalculatedValue();
				$_g = $_sheet_teknik->getCell("G".$j)->getCalculatedValue();
				$_h = $_sheet_teknik->getCell("H".$j)->getCalculatedValue();
				$_i = $_sheet_teknik->getCell("I".$j)->getCalculatedValue();
				$_j = $_sheet_teknik->getCell("J".$j)->getCalculatedValue();
				$_k = $_sheet_teknik->getCell("K".$j)->getCalculatedValue();
				$_l = $_sheet_teknik->getCell("L".$j)->getCalculatedValue();
				$_m = $_sheet_teknik->getCell("M".$j)->getCalculatedValue();
				$_n = $_sheet_teknik->getCell("N".$j)->getCalculatedValue();
				$_o = $_sheet_teknik->getCell("O".$j)->getCalculatedValue();
				$_p = $_sheet_teknik->getCell("P".$j)->getCalculatedValue();
				$_q = $_sheet_teknik->getCell("Q".$j)->getCalculatedValue();
				$_r = $_sheet_teknik->getCell("R".$j)->getCalculatedValue();
				$_s = $_sheet_teknik->getCell("S".$j)->getCalculatedValue();
				$_t = $_sheet_teknik->getCell("T".$j)->getCalculatedValue();
				$_u = $_sheet_teknik->getCell("U".$j)->getCalculatedValue();
				$_v = $_sheet_teknik->getCell("V".$j)->getCalculatedValue();

				if ( $_b != "" ) {
					$item = array();
					$item['peserta_ttl'] = $_c;
					$item['peserta_nama'] = strtoupper( $_b );
					$item['peserta_noinduk'] = $_d;
					$item['peserta_nisn'] = $_e;

					$item['peserta_jk'] = "";
					//$item['peserta_password'] = "1234";
					$item['peserta_jurusan'] = strtoupper( $this->_replaceJurusan( $_f ) );

					$item['peserta_nilai_agama'] = $_g;
					$item['peserta_nilai_pkn'] = $_h;
					$item['peserta_nilai_bindo'] = $_i;
					$item['peserta_nilai_mtk'] = $_j;
					$item['peserta_nilai_sind'] = $_k;
					$item['peserta_nilai_bing'] = $_l;

					$item['peserta_nilai_seni'] = $_m;
					$item['peserta_nilai_penjas'] = $_n;
					$item['peserta_nilai_blamp'] = $_o;

					$item['peserta_nilai_pak'] = $_p;
					$item['peserta_nilai_simdig'] = $_q;

					//jika teknik

					$item['peserta_nilai_fiska'] = $_r;
					$item['peserta_nilai_kimia'] = $_s;

					$item['peserta_nilai_c2'] = $_t;
					$item['peserta_nilai_c3'] = $_u;
					$item['peserta_nilai_rata2x'] = $_v;

					$item['peserta_jenis'] = "TEKNIK";

					//jika bismen

					$item['peserta_nilai_ipa'] = "";
					$item['peserta_nilai_ekonomi'] = "";
					$item['peserta_nilai_administrasi'] = "";


					$item['peserta_tahunajaran'] = "2021/2022";

					array_push($data,$item);
				}




			}

			if( sizeof($data) > 0 ){
				$this->db->insert_batch('peserta',$data);
				$result['pesan'] = "";
			}

		} else {
			$result['pesan'] = "Bukan File Excel...";
		}



		$this->output->set_header('Content-Type: application/json; charset=utf-8');
		echo json_encode($result);
    }


    function _replaceJurusan($jurusan){
    	if($jurusan == "AKL"){
			$jurusan = "Akutansi Keluangan Lembaga";
		}elseif($jurusan == "TKR"){
			$jurusan = "Teknik Kendaraan Ringan Otomotif";
		}elseif($jurusan == "TBSM"){
			$jurusan = "Teknik Bisnis Sepeda Motor";
		}elseif($jurusan == "OTKP"){
			$jurusan = "Otomasi Administrasi Perkantoran";
		}elseif($jurusan == "TKJ 1"){
			$jurusan = "Teknik Komputer dan Jaringan 1";
		}elseif($jurusan == "TKJ 2"){
			$jurusan = "Teknik Komputer dan Jaringan 2";
		}elseif($jurusan == "TKJ 3"){
			$jurusan = "Teknik Komputer dan Jaringan 3";
		}

    	return $jurusan;
	}

}
?>
