<?php

class Auth extends CI_Controller {

    function __construct() {
        parent::__construct();
		
    }

    function index(){
        $data = array();
        $data["title"] = "LOGIN | SMKN 1 CANDIPURO";
		
		$level = $this->session->userdata('level');
		if ( $level == 'admin' ) redirect('admin/dashboard');
		else $this->load->view('auth/login',$data);
		
    }
	
	function profile(){		
		
		if(!$this->session->userdata('user_level')){
			redirect('auth');
		}
		
		$users_data = $this->session->userdata();
		$users_data = $this->getUsersDetail($users_data);	
		
		$this->load->view('auth/profile',$users_data);
	}
	
	function sandi(){		
		
		if(!$this->session->userdata('user_level')){
			redirect('auth');
		}
		
		$users_data = $this->session->userdata();
		$users_data = $this->getUsersDetail($users_data);	
		
		$this->load->view('auth/sandi',$users_data);
	}
	
	function signin(){
		$username = $this->input->post('username');
		$password = $this->input->post('password');

        $now = date('Y-m-d H:i:s');


		$data = array();
		$data['success'] = false;

		if(empty($username) || empty($password)){			
			$data['pesan'] = 'Maaf! Username dan Password kosong!';
		}else{

		    if( $username == "admin" && $password == "admin"){
                $userdata = array();
                $userdata["username"] = "admin";
                $userdata["password"] = "admin";
                $userdata["level"] = "admin";

                $this->session->set_userdata($userdata);

                $data['pesan'] = 'Berhasil';
                $data['success'] = true;
                $data['redirect'] = 'admin/dashboard';

            }else{

                $this->db->where(array(
                    'peserta_nisn'=> $username,
                    'peserta_noinduk'=> $password
                ));

                $peserta = $this->db->get('peserta')->row_array();

                if ( !empty($peserta) && $peserta["peserta_id"] > 0 ) {

                    $userdata = array();
                    $userdata["username"] = $peserta["peserta_nisn"];
                    $userdata["password"] = $peserta["peserta_noinduk"];
                    $userdata["level"] = "peserta";



                    $this->db->where('peserta_id',$peserta["peserta_id"]);
                    $this->db->update('peserta',array('peserta_last_active' => $now));

                    $this->session->set_userdata($userdata);

                    $data['pesan'] = 'Berhasil';
                    $data['success'] = true;
                    $data['redirect'] = 'home/info';


                }else{

                    $data['pesan'] = 'Maaf! Username dan Password tidak sesuai!';
                    $data['success'] = false;
                    $data['redirect'] = 'home';

                }

            }


		}
		
		$this->output->set_header('Content-Type: application/json; charset=utf-8');
		echo json_encode($data);	
		
	}

    function logout() {
        $this->session->sess_destroy();
        redirect('home');
    }



    function buka(){
        $data = array();


        $this->peserta_username = $this->session->userdata('peserta_username');


        $data['success'] = false;
        if(!empty($this->peserta_username)){

            $data['pesan'] = 'Berhasil';
            $data['success'] = true;

        }

        $this->output->set_header('Content-Type: application/json; charset=utf-8');
        echo json_encode($data);
    }
	
}