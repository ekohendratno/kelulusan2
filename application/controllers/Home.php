<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	
	function __construct(){
		parent::__construct();	
		
		$this->load->model('Mymodel','m');
		$this->load->helpers('form');
		$this->load->helpers('url');




	}
	
	public function index(){

        $tanggal_sekarang = date('Y-m-d H:i:s');
        $tanggal_buka = "2022-06-03 22:30";  //2022-06-03 22:30   //2022-06-02 07:00


        $last_sesi_sekarang = date("Y-m-d H:i:s", strtotime($tanggal_sekarang));
        $last_sesi_buka = date("Y-m-d H:i:s", strtotime($tanggal_buka));


        $data = array();
        $data["title"] = "KELULUSAN | SMKN 1 CANDIPURO";

        $data["tanggal_buka"] = $tanggal_buka;


        $data["dibuka"] = 0;
        if( $last_sesi_sekarang > $last_sesi_buka ){
            $data["dibuka"] = 1;
        }

		$this->template->load('template_home','home/utama',$data);
	}

    public function info(){

        if($this->session->userdata('level') != 'peserta'){
            redirect('home');
        }


        $data = array();
        $data["title"] = "INFO | SMKN 1 CANDIPURO";

        $nisn = $this->session->userdata('username');

        $peserta = $this->db->get_where('peserta',array(
            'peserta_nisn'=>$nisn
        ))->result();

        $data["peserta"] = $peserta[0];

        $this->template->load('template_home','home/info', $data);
    }

    public function skl(){
        if($this->session->userdata('level') != 'peserta'){
            redirect('home');
        }


        $data = array();
        $data["title"] = "SKL | SMKN 1 CANDIPURO";

        $nisn = $this->session->userdata('username');

        $peserta = $this->db->get_where('peserta',array(
            'peserta_nisn'=>$nisn
        ))->result();

        $data["peserta"] = $peserta[0];
        $data["nosurat"] = "421.5/373/SMKN1CP/VI/2022";
        $data["kepsek"] = "SAHRIYANTO, S.Ag.,M.Pd.I";
        $data["kepsek_nip"] = "NIP.19731006 200003 1 003";
        $data["tanggal_ttd"] = "Candipuro, 3 Juni 2022";
        $data["tanggal_keputusan"] = " 31 Mei 2022";
        $data["sekolah"] = "SMKN 1 CANDIPURO";

        $this->load->view('home/skl',$data);
    }


}
