<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Level extends CI_Controller {
	
	function __construct(){
		parent::__construct();	
		
		$this->load->model('Mymodel','m');
		$this->load->helpers('form');
		$this->load->helpers('url');

        $this->sid = 0;
		
	}
	
	public function index(){

	    if( $this->input->get('agent') > 0 ){

            //$this->load->library('user_agent');

            //$this->browser = $this->agent->browser();
            //$this->browser_version = $this->agent->version();
            //$this->os = $this->agent->platform();
            //$this->ip_address = $this->input->ip_address();


            if ($this->agent->is_browser()){

                $this->agent = array(
                    'browser' => $this->agent->browser(),
                    'browser_version' => $this->agent->version(),
                    'os' => $this->agent->platform(),
                    'ip_address' => $this->input->ip_address()
                );

            }elseif ($this->agent->is_robot()){

                $this->agent = array(
                    'os' => $this->agent->platform(),
                    'ip_address' => $this->input->ip_address()
                );

            }elseif ($this->agent->is_mobile()){

                $this->agent = array(
                    'os' => $this->agent->platform(),
                    'ip_address' => $this->input->ip_address()
                );

            }else{
                $this->agent = 'Unidentified User Agent';
            }

            $sid = session_id();
            $this->sid = $this->session->userdata('sid');
            if(!$this->sid ) $this->session->set_userdata(array('sid'=>$sid));


        }
		
	}

	public function get(){
        $response = array();
        $response["response"] = array();

	    $this->db->select('*')->from('tts_available');
        $this->db->order_by('tts_level','asc');

	    $query = $this->db->get();

	    if($query->num_rows() > 0 ){
            $response["success"] = true;

            foreach($query->result() as $r ){

                //tts_level,quest_id,quest,answer,orientation,badge
                $array_push = array();
                $array_push['tts_tanggal']	= $r->tts_tanggal;
                $array_push['tts_level']= (int)$r->tts_level;
                $array_push['tts_col']	= (int)$r->tts_col;
                $array_push['tts_row']	= (int)$r->tts_row;
                $array_push["tts_available"] = array();


                $this->db->select('*')->from('tts')->where(array('tts_level' => (int)$r->tts_level));
                $this->db->order_by('quest_id','asc');
                $query2 = $this->db->get();

                foreach($query2->result() as $r2 ) {
                    $tts_available = array();

                    $tts_available['tts_number']	= $r2->tts_number;
                    $tts_available['quest_id']	    = (int)$r2->quest_id;
                    $tts_available['quest']		    = $r2->quest;
                    $tts_available['answer']		= $r2->answer;
                    $tts_available['orientation']	= (int)$r2->orientation;
                    $tts_available['badge']		    = (int)$r2->badge;
                    array_push($array_push['tts_available'], $tts_available);
                }

                array_push($response["response"], $array_push);
            }
        }else{
            $response["success"] = false;
            $response["response"] = "Tidak ditemukan data";
        }


        header('Content-type: application/json; charset=utf-8');
        echo json_encode($response);

    }

    public function download(){
	    $bahasa = $this->input->get('bahasa');
        if( $this->_if_table_exists($this->db->dbprefix."daerah_" . $bahasa) ){

            $response = array();
            $query = $this->db->query("SELECT * FROM ".$this->db->dbprefix."daerah_$bahasa");
            if ( $query->num_rows() > 0) {

                foreach ($query->result_array() as $r){

                    $array_push = array();
                    $array_push['id'] = $r['id'];
                    $array_push['indonesia'] = $r['indonesia'];
                    $array_push['daerah'] = $r['daerah'];
                    $array_push['aksara'] = $r['aksara'];

                    array_push($response, $array_push);
                }


                header('Content-type: application/json; charset=utf-8');
                header('Content-Disposition: attachment; filename=' . $bahasa . '.json');
                echo json_encode($response);
            }
        }
    }

    public function kata(){
        $response = array();
        $response["response"] = array();

        $bahasa = $this->input->get('bahasa');
        if( $this->_if_table_exists($this->db->dbprefix."daerah_" . $bahasa) ){

            $query = $this->db->query("SELECT * FROM ".$this->db->dbprefix."daerah_$bahasa");
            if ( $query->num_rows() > 0) {

                $response["success"] = true;

                foreach ($query->result_array() as $r){

                    $array_push = array();
                    $array_push['id'] = $r['id'];
                    $array_push['indonesia'] = $r['indonesia'];
                    $array_push['daerah'] = $r['daerah'];
                    $array_push['aksara'] = $r['aksara'];

                    array_push($response["response"], $array_push);
                }

            }else{
                $response["success"] = false;
                $response["response"] = "Tidak ditemukan data";
            }
        }else{
            $response["success"] = false;
            $response["response"] = "Bahasa tidak ditemukan";
        }

        header('Content-type: application/json; charset=utf-8');
        echo json_encode($response);
    }

    function _if_table_exists($bahasa){
        $query = $this->db->query("SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = '$bahasa'");

        if($query->num_rows() > 0){
            return true;
        }
        return false;
    }
	
	function cleanChar($text){
		
		/*$text = preg_replace("/[-]/"," ",$text);
		$text = preg_replace("/[^A-Za-z0-9 -_]/","",$text);
		$text = preg_replace("/[^A-Za-z0-9\s-_]/","",$text);
		$text = preg_replace("/[^A-Za-z0-9[:space:]-_]/","",$text);*/
		
		return $text;
	}
	
	function cleanCharFirst($text){
		
		$text = preg_replace("/[-]/", " - ", $text);
		$text = preg_replace("/[,]/", " , ", $text);
		$text = preg_replace("/[.]/", " . ", $text);
		$text = preg_replace("/[!]/", " ! ", $text);
		$text = preg_replace("/[?]/", " ? ", $text);
		$text = preg_replace('/["]/', ' " ', $text);
		$text = preg_replace("/[']/", " ' ", $text);
		$text = preg_replace("/[  ]/", " ", $text);
		$text = preg_replace("/[^A-Za-z0-9,.?!'\"@-_ ]/", "", $text);
		
		return $text;
	}

    function cleanCharFirst2($text){
        $text = preg_replace("/[-]/", "-", $text);
        $text = preg_replace("/[,]/", " , ", $text);
        $text = preg_replace("/[.]/", " . ", $text);
        $text = preg_replace("/[!]/", " ! ", $text);
        $text = preg_replace("/[?]/", " ? ", $text);
        $text = preg_replace('/["]/', " ' ", $text);
        $text = preg_replace("/[']/", " ' ", $text);
        $text = preg_replace("/[  ]/", " ", $text);
        $text = preg_replace("/[^A-Za-z0-9,.?!'\"@-_ ]/", "", $text);

        return $text;
    }
	
	
}
