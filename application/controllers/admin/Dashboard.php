<?php
class Dashboard extends CI_Controller{

    function __construct(){
        parent::__construct();
        $this->load->model('Mymodel','m');

        if($this->session->userdata('level') != 'admin'){
            redirect('home');
        }


    }

    function index(){
		$data['title'] = 'Dashboard Admin';


        $data['jumlah_peserta'] = 0;
        $data['jumlah_jurusan'] = 0;
        $data['jumlah_pelajaran'] = 0;
        $data['jumlah_koordinator'] = 0;
        $data['jumlah_pengajar'] = 0;
        $data['jumlah_soal'] = 0;
        $data['jumlah_ujian'] = 0;
        $data["tahunajaran"] = 0;
        $data["kegiatan"] = 0;

        $this->template->load('template','admin/dashboard',$data);
		
		if($this->session->userdata('level') != 'admin'){
			redirect('home');
		}
    }


}
?>