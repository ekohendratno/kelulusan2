<!DOCTYPE html>
<html lang="en">

<head>
    <title><?php echo $title;?></title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="<?php echo base_url();?>assets/img/iconsmk.png" />
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/select2.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/flipclock.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/util.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/main.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/iziToast.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/hover.css">
    <!--===============================================================================================-->
    <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async=""></script>
    <script>
        window.OneSignal = window.OneSignal || [];
        OneSignal.push(function() {
            OneSignal.init({
                appId: "f47baecf-f952-4051-9437-73b136bffc33",
            });
        });
    </script>

</head>

<body>

<div class="bg-img1 size1 overlay1 p-t-24" style="background-image: url('<?php echo base_url();?>assets/img/banner647.jpg');">
    <div class="flex-w flex-sb-m p-l-80 p-r-74 p-b-50 respon5">
        <div class="wrappic1 m-r-30 m-t-10 m-b-10">
            <a href="<?php echo base_url();?>"><img src="<?php echo base_url();?>assets/img/iconsmk.png" alt="LOGO" width="50"></a>
        </div>

        <div class="flex-w m-t-10 m-b-10">

            <a href="https://id-id.facebook.com/smkn1candipuro" class="size3 flex-c-m how-social trans-04 m-r-6 btn-circle">
                <i class="fa fa-facebook"></i>
            </a>

            <a href="https://www.youtube.com/c/SMKN1CANDIPURO" class="size3 flex-c-m how-social trans-04 m-r-6 btn-circle">
                <i class="fa fa-youtube-play"></i>
            </a>



        </div>
    </div>
    <div class="flex-w flex-sa p-r-200 respon1">
        <div class="bg0 wsize1 bor1 p-l-45 p-r-45 p-t-50 p-b-18 p-lr-15-sm">
            <h3 class="l1-txt3 txt-center p-b-43">
                LOGIN ADMIN
            </h3>

            <form id="form-login" class="w-full validate-form">

                <div class="wrap-input100 validate-input m-b-10" data-validate="Name is required">
                    <input class="input100 placeholder0 s1-txt1" type="text" name="username" placeholder="USER AKUN">
                    <span class="focus-input100"></span>
                </div>

                <div class="wrap-input100 validate-input m-b-20" data-validate="Password is required">
                    <input class="input100 placeholder0 s1-txt1" type="text" name="password" placeholder="KATA SANDI">
                    <span class="focus-input100"></span>
                </div>

                <button class="flex-c-m size2 s1-txt2 how-btn1 trans-04">
                    MASUK SEKARANG
                </button>
            </form>

            <p class="s1-txt3 txt-center p-l-15 p-r-15 p-t-25">
                Login menggunakan Username NISN dan Password NO INDUK
            </p>
        </div>
    </div>



    <div class="flex-w flex-sa p-r-200 respon1 text-center">
        <div class="p-b-60 respon3">
            <p style="color:#ffffff">Di desain oleh Eko Hendratno, S.Kom</p>
            <p style="color:#ffffff">&copy; SMKN1CANDIPURO</p>
        </div>



    </div>
</div>





<!--===============================================================================================-->
<script src="<?php echo base_url();?>assets/js/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
<script src="<?php echo base_url();?>assets/js/popper.js"></script>
<script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap.js"></script>
<!--===============================================================================================-->
<script src="<?php echo base_url();?>assets/js/select2.min.js"></script>
<!--===============================================================================================-->
<script src="<?php echo base_url();?>assets/js/flipclock.min.js"></script>
<script src="<?php echo base_url();?>assets/js/moment.min.js"></script>
<script src="<?php echo base_url();?>assets/js/moment-timezone.min.js"></script>
<script src="<?php echo base_url();?>assets/js/moment-timezone-with-data.min.js"></script>
<script src="<?php echo base_url();?>assets/js/countdowntime.js"></script>
<script src="<?php echo base_url();?>assets/js/iziToast.min.js"></script>
<script src="<?php echo base_url('assets/admin/js/sweetalert/sweetalert.min.js'); ?>"></script>
<!--===============================================================================================-->

<!--===============================================================================================-->
<script src="<?php echo base_url();?>assets/js/main.js"></script>
<script>
    $('#form-login').submit(function(e) {
        e.preventDefault();

        $.ajax({
            type: 'POST',
            url:'<?php echo base_url('index.php/auth/signin') ;?>',
            data: $(this).serialize(),
            dataType:'json',
            success: function(data) {

                if( data.success ){
                    swal({
                        title: "Berhasil!",
                        text: "Sedang di dialihkan, Tunggu beberapa saat!",
                        icon: "success",
                        button: false,
                    });

                    setTimeout(function () {
                        window.location.assign("<?php echo base_url();?>"+data.redirect);
                    },2000);


                }else{
                    swal({
                        title: "Gagal!",
                        text: data.pesan,
                        icon: "error",
                        button: true,
                    });
                }

            }
        });


        return false;
    });
</script>

</body>

</html>