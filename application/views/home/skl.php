

<!DOCTYPE html>
<html>

<head>
    <meta charset='utf-8'>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>SKL_<?php echo $peserta->peserta_nama;?></title>
    <link rel="icon" type="image/png" href="<?php echo base_url();?>assets/img/logo.png" />
    <script src="<?php echo base_url('assets/admin/js/jquery.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/admin/js/jquery-migrate-1.4.1.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/admin/js/qrcode.min.js') ?>"></script>
</head>
<body style="padding-left: 16px; padding-right: 16px;" >
<!--onload="window.print();"-->
<body style="font-size: 13px;">
<img src="<?php echo base_url();?>/assets/img/head.png" width="100%">


<center>
    <h4><u>SURAT KETERANGAN LULUS</u></h4>
    No. Surat : <?php echo $nosurat;?>    </center>

<div class="col-md-12">
    Yang Bertanda Tangan di Bawah Ini, Kepala <?php echo $sekolah;?>, Menerangkan Dengan Sesungguhnya Bahwa 		<br></br>
    <table style="margin-left: 80px;margin-right:80px" class="table table-sm table-bordered"cellpadding="1">
        <tr>
            <td>Nama Pesera Didik</td>
            <td> : </td>
            <td><?php echo $peserta->peserta_nama?></td>
        </tr>
        <tr>
            <td>Tempat, Tgl Lahir</td>
            <td> : </td>
            <td><?php echo $peserta->peserta_ttl?></td>
        </tr>
        <tr>
            <td>Nomor Induk Siswa Nasional</td>
            <td> : </td>
            <td><?php echo $peserta->peserta_nisn?></td>
        </tr>
        <tr>
            <td>Jurusan</td>
            <td> : </td>
            <td><?php echo ucwords(strtolower( $peserta->peserta_jurusan ))?></td>
        </tr>

    </table>
    <br></br>
    <p>Berdasarkan Hasil Keputusan Rapat Pleno Kelulusan Dewan Guru
        <?php echo $sekolah;?> Pada Hari Kamis, Tanggal <?php echo $tanggal_keputusan;?>, Maka Siswa yang bersangkutan di nyatakan </p>

    <center>
        <h2>
            <?php if($peserta->peserta_status == "LULUS") echo "LULUS / <strike>TIDAK LULUS</strike>"; else echo "<strike>LULUS</strike> / TIDAK LULUS";?>


        </h2>
    </center>
    <br></br>
    Demikian Surat keterangan Ini dibuat dengan sebenarnya untuk dipergunakan sebagaimana mestinya.       <br></br>
    <table width="100%">
        <tr>
            <td style="text-align: center; width:60%;">
                <div id="qrcode1"></div>
            </td>
            <td></td>
            <td style="text-align: left">
                <?php echo $tanggal_ttd;?>                     <p>Kepala <?php echo $sekolah;?> </p>
                <br><br><br>
                <strong><?php echo $kepsek;?></strong>                     <p><?php echo $kepsek_nip;?></p>
                <img style="z-index: 800;position:absolute;margin-top:-170px;margin-left:0px" class="img" src="<?php echo base_url();?>assets/img/ttd2.png" width="150">
                <img style="z-index: 920;position:absolute;margin-top:-180px;margin-left:-80px;opacity:0.7" class="img" src="<?php echo base_url();?>assets/img/cap3.png" width="150">
            </td>
        </tr>
    </table>
</div>
</body>

</br>

<div  style="margin-top: 7px; margin-left: 10px; margin-right: 10px; page-break-before:always;"></div>
<body style="font-size: 13px;">
<img src="<?php echo base_url();?>assets/img/head.png" width="100%">


<center>
    <h3 style="line-height: 10px"><u>SURAT KETERANGAN NILAI IJAZAH</u></h3>

    No. Surat : <?php echo $nosurat;?>
</center>
<!-- setting isinya disini -->

Yang Bertanda Tangan di Bawah Ini, Kepala <?php echo $sekolah;?>, Menerangkan Dengan Sesungguhnya Bahwa
</font>
<br></br>
<table style="margin-left: 20px;margin-right:80px" class="table table-sm table-bordered">
    <tr>
        <td>Nama Peserta Didik</td>
        <td> : </td>
        <td><?php echo $peserta->peserta_nama?></td>
    </tr>
    <tr>
        <td>Tempat, Tgl Lahir</td>
        <td> : </td>
        <td><?php echo $peserta->peserta_ttl?></td>
    </tr>
    <tr>
        <td>Nomor Induk Siswa Nasional</td>
        <td> : </td>
        <td><?php echo $peserta->peserta_nisn?></td>
    </tr>
    <tr>
        <td>Jurusan</td>
        <td> : </td>
        <td><?php echo ucwords(strtolower( $peserta->peserta_jurusan ))?></td>
    </tr>
    <tr>
        <td>Asal Sekolah </td>
        <td> : </td>
        <td><?php echo $sekolah;?></td>
    </tr>

    <tr>
        <td>Dinyatakan</td>
        <td> :</td>
        <td><strong>                                <strike>TIDAK LULUS</strike> / LULUS



</table>
<br>

Berdasarkan Hasil Keputusan Rapat Pleno Kelulusan Dewan Guru
<?php echo $sekolah;?> Pada Hari Kamis, Tanggal <?php echo $tanggal_keputusan;?>, Dengan Nilai Sebagai Berikut 				<br></br>

<table class="table table-bordered table-striped" border="1px" cellspacing="0" cellpadding="2" width="100%">
    <thead style="background-color:cornflowerblue;">
    <tr>
        <td align="center" width="50px"><strong>No</strong></td>
        <td align="center" width="500px"><strong>Mata Pelajaran</strong></td>

        <td align="center" width="140px"><strong>Nilai</strong></td>

        <!-- <td align="center" width="100px"><strong>NILAI UAMBN/UNBK</strong></td> -->
    </tr>
    </thead>
    <tbody>

    <tr>
        <td colspan="3"><strong>Kelompok A (Wajib)</strong></td>
    </tr>




    <tr>
        <td align="center">1.</td>
        <td> Pendidikan Agama dan Budi Pekerti</td>
        <td align="center"><?php echo round($peserta->peserta_nilai_agama,2);?></td>

    </tr>




    <tr>
        <td align="center">2.</td>
        <td> Pendidikan Pancasila dan Kewarganegaraan</td>
        <td align="center"><?php echo round($peserta->peserta_nilai_pkn,2);?></td>

        <!-- <td align="center"></td> -->
    </tr>
    <tr>
        <td align="center">3.</td>
        <td> Bahasa Indonesia</td>
        <td align="center"><?php echo round($peserta->peserta_nilai_bindo,2);?></td>

        <!-- <td align="center">77</td> -->
    </tr>


    <tr>
        <td align="center">4.</td>
        <td> Matematika</td>
        <td align="center"><?php echo round($peserta->peserta_nilai_mtk,2);?></td>

        <!-- <td align="center">77</td> -->
    </tr>


    <tr>
        <td align="center">5.</td>
        <td> Sejarah Indonesia</td>
        <td align="center"><?php echo round($peserta->peserta_nilai_sind,2);?></td>

        <!-- <td align="center">77</td> -->
    </tr>

    <tr>
        <td align="center">6.</td>
        <td> Bahasa Inggris</td>
        <td align="center"><?php echo round($peserta->peserta_nilai_bing,2);?></td>

        <!-- <td align="center">77</td> -->
    </tr>

    <tr>
        <td colspan="3"><strong>Muatan Kewilayahan</strong></td>
    </tr>
    <tr>
        <td align="center">1.</td>
        <td> Seni Budaya</td>
        <td align="center"><?php echo round($peserta->peserta_nilai_seni,2);?></td>

        <!-- <td align="center"></td> -->
    </tr>
    <tr>
        <td align="center">2.</td>
        <td> Pendidikan Jasmani, Olahraga dan Kesehatan </td>
        <td align="center"><?php echo round($peserta->peserta_nilai_penjas,2);?></td>

        <!-- <td align="center"></td> -->
    </tr>

    <tr>
        <td colspan="3"><strong>C1. Dasar Bidang Keahlian</strong></td>
    </tr>
    <tr>
        <td align="center">1.</td>
        <td> Simulasi dan Komunikasi Digital</td>
        <td align="center"><?php echo round($peserta->peserta_nilai_simdig,2);?></td>

        <!-- <td align="center"></td> -->
    </tr>

    <?php if($peserta->peserta_jenis == "TEKNIK"):?>


        <tr>
            <td align="center">2.</td>
            <td> Fisika</td>
            <td align="center"><?php echo round($peserta->peserta_nilai_fiska,2);?></td>

            <!-- <td align="center"></td> -->
        </tr>
        <tr>
            <td align="center">3.</td>
            <td> Kimia</td>
            <td align="center"><?php echo round($peserta->peserta_nilai_kimia,2);?></td>

            <!-- <td align="center"></td> -->
        </tr>


    <?php elseif($peserta->peserta_jenis == "BISMEN"):?>


        <tr>
            <td align="center">2.</td>
            <td> Ilmu Pengetahuan Alam</td>
            <td align="center"><?php echo round($peserta->peserta_nilai_ipa,2);?></td>

            <!-- <td align="center"></td> -->
        </tr>
        <tr>
            <td align="center">3.</td>
            <td> Ekonomi Bisnis</td>
            <td align="center"><?php echo round($peserta->peserta_nilai_ekonomi,2);?></td>

            <!-- <td align="center"></td> -->
        </tr>
        <tr>
            <td align="center">4.</td>
            <td> Administrasi Umum</td>
            <td align="center"><?php echo round($peserta->peserta_nilai_administrasi,2);?></td>

            <!-- <td align="center"></td> -->
        </tr>

    <?php endif;?>

    <tr>
        <td colspan="2"> <strong>C2. Dasar Program Keahlian</strong></td>
        <td align="center"><?php echo round($peserta->peserta_nilai_c2,2);?></td>

        <!-- <td align="center"></td> -->
    </tr>
    <tr>
        <td colspan="2"> <strong>C3. Paket Keahlian</strong></td>
        <td align="center"><?php echo round($peserta->peserta_nilai_c3,2);?></td>

        <!-- <td align="center"></td> -->
    </tr>



    <tr>
        <td colspan="3"><strong>Muatan Lokal</strong></td>
    </tr>
    <tr>
        <td align="center">1.</td>
        <td> Pendidikan Bahasa Lampung</td>
        <td align="center"><?php echo round($peserta->peserta_nilai_blamp,2);?></td>

        <!-- <td align="center"></td> -->
    </tr>
    <tr>
        <td align="center">2.</td>
        <td> Pendidikan Anti Korupsi </td>
        <td align="center"><?php echo round($peserta->peserta_nilai_pak,2);?></td>

        <!-- <td align="center"></td> -->
    </tr>



    </tr>









    <tr style="background-color:lightsteelblue;">
        <td colspan="2"><b>NILAI RATA RATA </b></td>
        <td align="center"><strong><?php echo round($peserta->peserta_nilai_rata2x,2);?></strong></td>

        <!-- <td align="center"></td> -->

    </tbody>
</table>
</div>

<br>

<p>Surat Keterangan Lulus ini Berlaku Sementara Sampai Dengan Diterbitkannya
    Ijazah Kepada Yang Bersangkutan, Untuk Menjadikan Maklum Bagi Yang Berkepentingan.</p>

</font>

<table width="100%">
    <tr>
        <td style="text-align: center; width:60%;">
            <div id="qrcode2"></div>
        </td>
        <td></td>
        <td style="text-align: left">
            <?php echo $tanggal_ttd;?>                     <p>Kepala <?php echo $sekolah;?> </p>
            <br><br><br>
            <strong><?php echo $kepsek;?></strong>                      <p><?php echo $kepsek_nip;?></p>
            <img style="z-index: 800;position:absolute;margin-top:-170px;margin-left:0px" class="img" src="<?php echo base_url();?>assets/img/ttd2.png" width="150">
            <img style="z-index: 920;position:absolute;margin-top:-180px;margin-left:-80px;opacity:0.7" class="img" src="<?php echo base_url();?>assets/img/cap3.png" width="150">
        </td>
    </tr>
</table>


<!-- batas======================================-->





<script>

    new QRCode(document.getElementById("qrcode1"), {
        text: "<?php echo $peserta->peserta_nisn?>",
        width: 100,
        height: 100,
        colorDark : "#2d2d2d",
        colorLight : "#ffffff",
        correctLevel : QRCode.CorrectLevel.H
    });

    new QRCode(document.getElementById("qrcode2"), {
        text: "<?php echo $peserta->peserta_nisn?>",
        width: 100,
        height: 100,
        colorDark : "#2d2d2d",
        colorLight : "#ffffff",
        correctLevel : QRCode.CorrectLevel.H
    });




    window.print();
</script>

</body>
</html>


