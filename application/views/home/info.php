
<div class="bg-img1 size1 overlay1 p-t-24" style="background-image: url('<?php echo base_url();?>assets/img/banner647.jpg');">
    <div class="flex-w flex-sb-m p-l-80 p-r-74 p-b-50 respon5">
        <div class="wrappic1 m-r-30 m-t-10 m-b-10">
            <a href="<?php echo base_url();?>"><img src="<?php echo base_url();?>assets/img/iconsmk.png" alt="LOGO" width="50"></a>
        </div>

        <div class="flex-w m-t-10 m-b-10">
            <div class="animated flipInX hvr-pulse">

                <a href="javascript:void()" title="Keluar" onclick="aksiLogout()" style="font-size: 21px;color:white" class=" flex-c-m  trans-04 m-l-6 m-r-6">
                    <span class="m-l-5">Keluar</span>
                </a>
            </div>
        </div>
    </div>
    <div class="flex-w flex-sa p-r-200 respon1">
        <div class="p-b-60 respon3">

            <div style="font-size:40px" class="animated bounceInLeft l1-txt2 p-b-45 respon2 respon4 text-center"><?php echo $peserta->peserta_nama;?></div>
            <p id='pbuka' class="animated flipInX l1-txt1 p-b-10 respon2 text-center">
                Silahkan buka Amplopnya ...
            </p>

            <p id="plulus" class="animated flipInX l1-txt1 p-b-10 respon2 text-center">SELAMAT KAMU <?php if($peserta->peserta_status == "LULUS") echo "LULUS"; else echo "TIDAK LULUS";?> NAK!</p>
            <p id="plulus_pesan"  class="animated flipInX p-b-10 respon2 text-center">
                <?php if($peserta->peserta_status == "LULUS"):?>
                    <i>Teruslah belajar, berkarya, dan jadilah pribadi penebar manfaat.</i>
                <?php else:?>
                    <i>Jangan berkecil hati tetap teruslah belajar, raih harapanmu.</i>
                <?php endif;?>
            </p>




            <div id="formamplop">
                <div class="animated tada  p-l-45 p-r-45 p-b-10 p-lr-15-sm">
                    <div class="loader text-center"><img src="<?php echo base_url();?>assets/img/loading.gif"></div>
                    <a id="amploptutup" class="animated tada hvr-pulse" href="#">
                        <img src="<?php echo base_url();?>assets/img/amploptutup.png" alt="LOGO" style="width:100%;max-width:400px;" />
                    </a>
                    <a id="amplopbuka" class="animated tada p-b-10" href="#">
                        <img src="<?php echo base_url();?>assets/img/amplopbuka.png" alt="LOGO" style="width:100%;max-width:400px;" />
                    </a>
                </div>
                <span id="keterangan" class="keter  animated tada "><?php if($peserta->peserta_status == "LULUS") echo "LULUS"; else echo "TIDAK LULUS";?></span>
            </div>

            <div class="form-group" id="formamplop_tombol">


                <?php if($peserta->peserta_status == "LULUS"):?>

                    <button data-backdrop="static" data-keyboard="false" href="#formPengumuman" data-toggle="modal" style="background:white ;color:#000;height:50px" class="hvr-grow flex-c-m btn-block s1-txt2 how-btn1 m-t-30 trans-04">
                        Pengumuman
                    </button>
                    <a target="_blank" href="<?php echo base_url('index.php/home/skl') ;?>" id="print">
                        <button style="background: blue;height:50px" class="hvr-grow btn-block flex-c-m s1-txt2 how-btn1 m-t-30 trans-04">Print SKL</button>
                    </a>

                <?php endif;?>


            </div>

        </div>

    </div>



    <div class="flex-w flex-sa p-r-200 respon1 text-center">
        <div class="p-b-60 respon3">
            <p style="color:#ffffff">Di desain oleh Eko Hendratno, S.Kom</p>
            <p style="color:#ffffff">&copy; SMKN1CANDIPURO</p>
        </div>



    </div>

</div>

<div class="modal" tabindex="-1" role="dialog" id="formPengumuman">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Kasih info maseh</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <p>Untuk pengambilan Surat Keterangan Lulus (SKL) bisa diambil disekolah mulai hari Senin 4 Juni 2022</p>

            </div>
            <div class="modal-footer">
                <a target="_blank" href="<?php echo base_url('index.php/home/skl') ;?>" id="print">
                    <button type="button" class="btn btn-primary">Print SKL</button>
                </a>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script>

    $("#amploptutup").click(function(e) {
        e.preventDefault();
        $(this).hide();
        $(".loader").show();
        $.ajax({
            type: 'POST',
            url:'<?php echo base_url('index.php/auth/buka') ;?>',
            data: "status=1",
            dataType:'json',
            success: function(data) {

                setTimeout(function() {
                    $(".loader").hide();
                    $('#amplopbuka').show();
                    $("#keterangan").show();
                    $("#pbuka").hide();
                    $("#plulus").show();
                    $("#plulus_pesan").show();
                    $("#formamplop_tombol").show();
                }, 5000);

            }
        });
    });

    $(".loader").hide();
    $("#plulus").hide();
    $("#plulus_pesan").hide();
    $("#formamplop_tombol").hide();
    $("#amplopbuka").hide();
    $("#keterangan").hide();


    function aksiLogout() {
        swal({
            title: "Keluar?",
            text: "Kamu yakin mau keluar?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    window.location.assign("<?php echo base_url();?>auth/logout");
                }

            });
    }

</script>