<div class="bg-img1 size1 overlay1 p-t-24" style="background-image: url('<?php echo base_url();?>assets/img/banner647.jpg');">
    <div class="flex-w flex-sb-m p-l-80 p-r-74 p-b-50 respon5">
        <div class="wrappic1 m-r-30 m-t-10 m-b-10">
            <a href="<?php echo base_url();?>"><img src="<?php echo base_url();?>assets/img/iconsmk.png" alt="LOGO" width="50"></a>
        </div>

        <div class="flex-w m-t-10 m-b-10">

            <a href="https://id-id.facebook.com/smkn1candipuro" class="size3 flex-c-m how-social trans-04 m-r-6 btn-circle">
                <i class="fa fa-facebook"></i>
            </a>

            <a href="https://www.youtube.com/c/SMKN1CANDIPURO" class="size3 flex-c-m how-social trans-04 m-r-6 btn-circle">
                <i class="fa fa-youtube-play"></i>
            </a>



        </div>
    </div>
    <div class="flex-w flex-sa p-r-200 respon1">
        <div class="animated bounceInLeft p-t-34 p-b-10 respon3">
            <p class="l1-txt1 p-b-10 respon2">
                Pengumuman Kelulusan
            </p>
            <h3 class="l1-txt2 p-b-10 respon2 respon4">
                <?php if($dibuka == 1): echo "Sudah Dibuka"; else: echo "DiBuka Dalam"; endif;?>
            </h3>


            <div class="cd100" <?php if($dibuka == 1) echo "style=\"display: none\"";?>></div>


        </div>

        <?php if($dibuka == 1):?>

            <div class="animated bounceInRight bg0 wsize1 bor1 p-l-45 p-r-45 p-t-50 p-b-18 p-lr-15-sm">
                <h3 class="l1-txt3 txt-center p-b-43">
                    LOGIN
                </h3>

                <form id="form-login" class="w-full validate-form">

                    <div class="wrap-input100 validate-input m-b-10" data-validate="Name is required">
                        <input class="input100 placeholder0 s1-txt1" type="text" name="username" placeholder="NOMOR NISN">
                        <span class="focus-input100"></span>
                    </div>

                    <div class="wrap-input100 validate-input m-b-20" data-validate="Password is required">
                        <input class="input100 placeholder0 s1-txt1" type="text" name="password" placeholder="KATA SANDI">
                        <span class="focus-input100"></span>
                    </div>

                    <button class="flex-c-m size2 s1-txt2 how-btn1 trans-04">
                        MASUK SEKARANG
                    </button>
                </form>

                <p class="s1-txt3 txt-center p-l-15 p-r-15 p-t-25">
                    Login menggunakan Username NISN dan Password NO INDUK
                </p>
            </div>


        <?php endif?>
    </div>



    <div class="flex-w flex-sa p-r-200 respon1 text-center">
        <div class="p-b-60 respon3">
            <p style="color:#ffffff">Di desain oleh Eko Hendratno, S.Kom</p>
            <p style="color:#ffffff">&copy; SMKN1CANDIPURO</p>
        </div>



    </div>
</div>
<script>

    var countDownDate = new Date("<?php echo $tanggal_buka;?>").getTime();
    var now = new Date().getTime();

    var distance = countDownDate - now;

    // Time calculations for days, hours, minutes and seconds
    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);

    $('.cd100').countdown100({
        /*Set Endtime here*/
        /*Endtime must be > current time*/
        endtimeMonth: 0,
        endtimeDate: days,
        endtimeHours: hours,
        endtimeMinutes: minutes,
        endtimeSeconds: seconds,
        timeZone: ""
        // ex:  timeZone: "America/New_York"
        //go to " http://momentjs.com/timezone/ " to get timezone
    });


    counterDown();
    function counterDown() {
        var countDownDate = new Date("<?php echo $tanggal_buka;?>").getTime();

        var x = setInterval(function() {

            var now = new Date().getTime();

            var distance = countDownDate - now;
            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);

            // If the count down is finished, write some text
            if (days == 0 && hours == 0 && minutes == 0 && seconds == 0 ) {

                setTimeout(function() {
                    window.location.assign("<?php echo base_url();?>");
                }, 3000);

            }
        }, 1000);

    }

    $('#form-login').submit(function(e) {
        e.preventDefault();

        $.ajax({
            type: 'POST',
            url:'<?php echo base_url('index.php/auth/signin') ;?>',
            data: $(this).serialize(),
            dataType:'json',
            success: function(data) {

                if( data.success ){
                    swal({
                        title: "Berhasil!",
                        text: "Sedang di dialihkan, Tunggu beberapa saat!",
                        icon: "success",
                        button: false,
                    });

                    setTimeout(function () {
                        window.location.assign("<?php echo base_url();?>"+data.redirect);
                    },2000);


                }else{
                    swal({
                        title: "Gagal!",
                        text: data.pesan,
                        icon: "error",
                        button: true,
                    });
                }

            }
        });


        return false;
    });
</script>