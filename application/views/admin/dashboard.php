<div class="position-absolute">
    <div class="container container-medium">
        <div class="row">

            <div class="col-sm-4 col-xs-12">
                <div class="small-box bg-light">
                    <div class="icon">
                        <i class="fas fa-chalkboard-teacher"></i>
                    </div>
                    <div class="inner">
                        <h3><?php echo $jumlah_koordinator;?></h3>
                        <p>Guru Koordinator Soal</p>
                    </div>
                </div>
            </div>


            <div class="col-sm-8 col-xs-12">
                <div class="small-box bg-light">
                    <div class="icon">
                        <i class="fas fa-sun"></i>
                    </div>
                    <div class="inner">
                        <h3><?php echo $tahunajaran;?>-<?php echo $kegiatan;?></h3>
                        <p>Tahun Ajaran</p>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<div class="container container-medium" style="margin-top: 20px">
    <div class="col-md-12">
        <div class="row">

            <div class="panel panel-default">

                <div class="panel-body">
                    <div class="homes" style="padding-bottom: 40px; padding-top: 40px">

                        <div class="col-lg-3 col-sm-3 col-xs-6 text-center">
                            <a href="<?php echo base_url()?>admin/peserta">
                                <div class="dashboard-circle">
                                    <div class="icon">
                                        <i class="fas fa-users"></i>
                                    </div>
                                    <p class="text-center">Peserta</p>
                                </div>
                            </a>
                        </div>

                        <div class="col-lg-3 col-sm-3 col-xs-6 text-center">
                            <a href="<?php echo base_url()?>admin/pengaturan">
                                <div class="dashboard-circle">
                                    <div class="icon">
                                        <i class="fas fa-gear"></i>
                                    </div>
                                    <p class="text-center">Pengaturan</p>
                                </div>
                            </a>
                        </div>

                        <div class="col-lg-3 col-sm-3 col-xs-6 text-center">
                            <a href="<?php echo base_url()?>admin/pesan">
                                <div class="dashboard-circle">
                                    <div class="icon">
                                        <i class="fas fa-bullhorn"></i>
                                    </div>
                                    <p class="text-center">Pesan</p>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-3 col-sm-3 col-xs-6 text-center">
                            <a href="<?php echo base_url()?>admin/signout">
                                <div class="dashboard-circle">
                                    <div class="icon">
                                        <i class="fas fa-sign-out"></i>
                                    </div>
                                    <p class="text-center">Keluar</p>
                                </div>
                            </a>
                        </div>

                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>


</div>



<style type="text/css">
    body {
        font-family: sans-serif;
        color: #514d6a;
        font-size: 1.5em;
        overflow-x: hidden;
        background-color: #ddd;
    }
    nav.navbar {
        box-shadow: 2px 2px 2px 2px rgb(0 0 0 / 0%);
    }
    .position-absolute{
        margin-top: -20px;
        padding-top: 20px;
        background: #778e9a!important;
        box-shadow: 2px 2px 2px 2px rgb(0 0 0 / 11%);
    }
    .small-box .svg-inline--fa{
        font-size: 28px;
        color: #778e9a;
    }
    .small-box p{
        color: rgba(110, 110, 110, 0.69);
    }


    .homes .img-responsive {
        margin: 0 auto;
    }
    .homes p{
        color: #5B5B5B;
    }
    .homes .icon {
        -webkit-transition: all .3s linear;
        -o-transition: all .3s linear;
        transition: all .3s linear;
        text-align: center;
        z-index: 0;
        font-size: 50px;
        color: rgba(0,0,0,0.15);
    }
    .homes .icon:hover {
        text-decoration: none;
        color: #0092f9;
    }

    .img-home{
        padding: 10px;
    }


</style>

<script src="<?php echo base_url();?>assets/admin/js/tinymce/tinymce.min.js"></script>
<link href="<?php echo base_url();?>assets/css/timeline.css" rel="stylesheet">

<script type="text/javascript">
    tinyMCE.init({
        selector: "#pesan_text",
        height: 300,
        min_height: 300,
        menubar: false,statusbar:false,
        plugins: 'autoresize searchreplace autolink directionality visualblocks visualchars fullscreen link table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern help',
        toolbar: 'alignleft alignright bullist numlist table forecolor backcolor link removeformat',
    });


    $('#untuk').selectpicker('refresh');



    $("#pengaturanpesan").slideToggle();
    $(".submitpengaturanpesan").click(function(){
        var side = $(".submitpengaturanpesan .fa-arrow-up").attr('class');
        if(side){
            $(".submitpengaturanpesan .fa-arrow-up").removeClass("fa-arrow-up").addClass("fa-arrow-down");
        }else{
            $(".submitpengaturanpesan .fa-arrow-down").removeClass("fa-arrow-down").addClass("fa-arrow-up");
        }

        $("#pengaturanpesan").slideToggle();
        $(".submitpesan").slideToggle();

    });


    //$('.position-absolute').detach().appendTo( $('nav.navbar') );
    <?php if(!empty($tahunajaran) ){?>
    $('#tahunajaran').val('<?php echo $tahunajaran;?>');
    $('#tahunajaran').selectpicker('refresh');

    $('#kegiatan').val('<?php echo $kegiatan;?>');
    $('#kegiatan').selectpicker('refresh');

    $('#formTahunAjaran').modal('hide');
    <?php }else{?>
    $('#formTahunAjaran').modal('show');
    <?php }?>

    function closetahunajaran() {
        $('#closetahunajaran').show();
    }

    function submitTahunAjaran(){

        var tahunajaran = $("#tahunajaran").val();
        var kegiatan = $("#kegiatan").val();

        $.ajax({
            type:'POST',
            data: "tahunajaran="+tahunajaran+"&kegiatan="+kegiatan,
            url:'<?php echo base_url('index.php/admin/pengaturan/simpantahunajaran') ;?>',
            beforeSend: function () {
                $('#loading_ajax').show();
            },
            success: function(){
                $('#formTahunAjaran').modal('hide');
                $('#loading_ajax').fadeOut("slow");

                setTimeout(function() {
                    window.location.assign("<?php echo base_url('index.php/admin/dashboard') ;?>");
                }, 300);
            }
        });
    }



    $('#loading_ajax').fadeOut("slow");
    $('#pesan-refresh').click(function(){
        searchFilterPesan(0);
    });

    searchFilterPesan(0);
    function searchFilterPesan() {
        $.ajax({
            type: 'POST',
            url: '<?php echo base_url(); ?>index.php/admin/pesan/timeline/',
            dataType:'json',
            beforeSend: function () {
                $('#loading_ajax').show();
            },
            success: function (responseData) {
                paginationDataPesan(responseData);
                $('#loading_ajax').fadeOut("slow");
            }
        });
    }

    function paginationDataPesan(data) {
        $('ul.timeline').empty();
        $('ul.timeline').append('<li class="time-label"><span class="btn btn-sm btn-success">Pengumuman Terakhir</span></li>');
        for(emp in data){
            var empRow = '<li><i class="glyphicon glyphicon-envelope"></i>'+
                '<div class="timeline-item">'+
                '<h3 class="timeline-header" style="font-size: 12px;"><i class="fas fa-calendar-alt"></i> '+data[emp].pesan_tanggal+' dari '+data[emp].username+'</h3>'+
                '<div class="timeline-body">'+data[emp].pesan_text+'</div>'+
                '<div class="timeline-footer"><a onclick="submitHapusPesan('+data[emp].pesan_id+')" class="btn btn-sm btn-circle btn-danger"><i class="glyphicon glyphicon-trash"></i></a></div>'+
                '</div>'+
                '</li>';
            $('ul.timeline').append(empRow);
        }
        $('ul.timeline').append('<li><i class="glyphicon glyphicon-time"></i></li>');
    }



    function submitPesan(){


        var pesan_text =  tinyMCE.get("pesan_text").getContent();

        var untuk =  $("[name='untuk']").val();
        var kelas_sekarang =  $("[name='kelas_sekarang']").val();
        var jurusan_id =  $("[name='jurusan_id']").val();
        var ruang =  $("[name='ruang']").val();

        $.ajax({
            type:'POST',
            data: {
                pesan_text:pesan_text,
                untuk:untuk,
                kelas_sekarang:kelas_sekarang,
                jurusan_id:jurusan_id,
                ruang:ruang
            },
            url:'<?php echo base_url('index.php/admin/pesan/tambahdata') ;?>',
            dataType:'json',
            success: function(hasil){

                searchFilterPesan(0);

                //$("#form1Pesan").modal("hide");


                $('#loading_ajax').fadeOut("slow");
                $('#Notifikasi').html('<p class="alert alert-success">Pesan berhasil dikirim</p>');
                $("#Notifikasi").fadeIn('fast').show().delay(3000).fadeOut('fast');

                $("#pengaturanpesan").slideToggle();
                if(hasil.pesan == ''){
                    //window.location.assign("<?php echo base_url();?>index.php/admin/pesan");
                }
            }
        });
    }

    function submitHapusPesan(x){
        var tanya = confirm('Apakah yakin mau hapus data?');
        if(tanya){
            $.ajax({
                type:'POST',
                data: 'id='+x,
                url:'<?php echo base_url('index.php/admin/pesan/hapusdatabyid') ;?>',
                success: function(){
                    searchFilterPesan(0);
                }
            });
        }
    }
</script>