<!DOCTYPE html>
<html lang="en">

<head>
    <title><?php echo $title;?></title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="<?php echo base_url();?>assets/img/iconsmk.png" />
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/select2.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/flipclock.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/util.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/main.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/iziToast.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/hover.css">

    <!--===============================================================================================-->
    <script src="<?php echo base_url();?>assets/js/jquery-3.2.1.min.js"></script>
    <!--===============================================================================================-->
    <script src="<?php echo base_url();?>assets/js/popper.js"></script>
    <script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap.js"></script>
    <!--===============================================================================================-->
    <script src="<?php echo base_url();?>assets/js/select2.min.js"></script>
    <!--===============================================================================================-->
    <script src="<?php echo base_url();?>assets/js/flipclock.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/moment.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/moment-timezone.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/moment-timezone-with-data.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/countdowntime.js"></script>
    <script src="<?php echo base_url();?>assets/js/iziToast.min.js"></script>
    <script src="<?php echo base_url('assets/admin/js/sweetalert/sweetalert.min.js'); ?>"></script>

    <!--===============================================================================================-->

    <script src="<?php echo base_url();?>assets/js/main.js"></script>

    <!--===============================================================================================-->

    <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async=""></script>
    <script>
        window.OneSignal = window.OneSignal || [];
        OneSignal.push(function() {
            OneSignal.init({
                appId: "f47baecf-f952-4051-9437-73b136bffc33",
            });
        });
    </script>

</head>

<body>

<?php echo $contents ?>

</body>

</html>