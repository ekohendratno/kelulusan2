<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Kelulusan 2021/2022</title>
    <script src="<?php echo base_url('assets/js/jquery.min.js') ?>"></script>	
    <script src="<?php echo base_url('assets/js/jquery-ui.js') ?>"></script>
    <link href="<?php echo base_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/jquery-ui.css') ?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/custom-home.css') ?>" rel="stylesheet">
	<script type="text/javascript">
        $(document).ready(function(){
            $('#result-a').hide();
            $('#timeShow').hide();
        });
		function searchName(){
			var nama = $('#nama').val();

            $.ajax({
                type:'POST',
                data: 'keywords='+nama,
                url:'<?php echo base_url('home/ambildatabynama') ;?>',
                dataType:'json',
                beforeSend: function () {
                    $('#loading_ajax').show();
                },
                success: function(hasil){
                    console.log(hasil);
                    $('#loading_ajax').hide();
                    paginationDataHistory(hasil);
                    $('#result-a').show();
                },
                error: function () {
                    alert("Maaf terjadi gangguan pada server!");

                }
            });
		}

        $('#status_link').show();
        $('#status').hide();
        function statusShow(){
            $('#status_link').hide();
            $('#status').show();
        }



		function paginationDataHistory(data) {
            $('#postList tbody').empty();
            for(emp in data){

                var empRow = '<tr>'+
                    '<td class="text-center" width="15">'+data[emp].siswa_nomor+'</td>'+
                    '<td><center><img src="<?php echo base_url('uploads/users/avatar.png');?>" style="width: 80px;"/></center></td>'+
                    '<td class="text-left">'+data[emp].siswa_nama+'<br/>T.A '+data[emp].siswa_ta+'</td>'+
                    '<td class="text-center"><a id="status_link" href="#" onClick="statusShow()">Tampilkan</a><div id="status" style="display:none;"><strong>'+data[emp].siswa_status+'</strong></div></td>'+
                    '</tr>';
                $('#postList tbody').append(empRow);
            }
		}

        counterDown();
		function counterDown() {
            $('#timeShowCounter').empty();
            var countDownDate = new Date("May 31, 2022 23:00:00").getTime();

            var x = setInterval(function() {

                var now = new Date().getTime();

                var distance = countDownDate - now;

                // Time calculations for days, hours, minutes and seconds
                var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                var seconds = Math.floor((distance % (1000 * 60)) / 1000);


                if(hours > 0){
                    $("#timeShowCounter").html(hours + " jam, " + minutes + " menit, " + seconds +" detik");
                }else if(minutes > 0) {
                    $("#timeShowCounter").html(minutes + " menit, " + seconds + " detik");
                }else if(seconds > 0) {
                    $("#timeShowCounter").html("Sisa " + seconds + " detik lagi");
                }

                //$('#timeShowCounter').html( hours + " jam "
                    //+ minutes + " menit " + seconds + " detik lagi " );
                // If the count down is finished, write some text
                if (distance < 0) {
                    //clearInterval(x);
                    //document.getElementById("counterWaktu").innerHTML = "EXPIRED";

                    $('#timeShowCounter').hide();
                    $('#timeShow').show();
                }
            }, 1000);
        }



	</script>
  </head>

  <body class="text-center" style="padding-top: 10px;">
  
	<div id="loading_ajax"><center style="padding:20px;"><div class="_ani_loading"><span style="clear:both">Memuat...</span></div></center></div>
	  
	  <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?php echo base_url();?>" title="Kelulusan">
            Kelulusan
          </a>
        </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="https://berkarya.kopas.id/privacy-policy/" target="_blank">Privacy Policy</a></li>
                        <li><a href="https://berkarya.kopas.id/abouts/" target="_blank">Abouts</a></li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div>
        </nav>
	  
        <div class="container" style="margin-top:120px;">

            <div class="row" >
                <div id="loginbox" class="mainbox col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2">
                <div class="col-lg-12">


                    <div style="font-size: 18px">
                        Pengumuman Kelulusan
                        <h1 id="timeShowText" style="font-size: 58px">DIBUKA DALAM</h1>
                    </div>
                    <div id="timeShowCounter" style="font-size: 42px; font-weight: bold;"></div>
                    <div id="timeShow">

                        <div class="input-group  input-group-lg">
                            <input type="text" id="nama" name="nama" class="form-control" placeholder="Ketik namamu disini...">
                            <span class="input-group-btn">
                            <button class="btn btn-default" type="button" onClick="searchName()">Cari</button>
                        </span>
                        </div><!-- /input-group -->


                        <div class="clearfix" style="margin-bottom: 20px"></div>

                        <div class="row" id="result-a">

                            <table id='postList' class="table table-striped table-hover table-bordered">
                                <thead>
                                <tr>
                                    <th class="text-center" width="15">NO</th>
                                    <th class="text-center">FOTO</th>
                                    <th class="text-left">NAMA</th>
                                    <th class="text-center">STATUS KELULUSAN</th>
                                </tr>
                                </thead>
                                <tbody></tbody>
                            </table>


                        </div>

                    </div>


                </div><!-- /.col-lg-6 -->
                </div>
            </div><!-- /.row -->

            <div class="clearfix" style="margin-bottom: 120px"></div>



            <footer class="mastfoot mt-auto" style="margin-top:70px;">
                <div class="inner">
                    <p class="text-dark">Copyright 2018. Versi 1. Powered by <a href="https://berkarya.kopas.id/">@KopasProjects</a>.</p>
                </div>
            </footer>



        </div>
        <script src="<?php echo base_url('assets/js/bootstrap.min.js') ?>"></script>
  </body>
</html>
